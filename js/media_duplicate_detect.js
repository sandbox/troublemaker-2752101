(function ($) {

Drupal.behaviors.duplicateDetect = {
  attach: function (context, settings) {
    $('#dupe-file-select').click(function(){
      existingFile = Drupal.settings.duplicateDetect.file;
      if (existingFile) {
        Drupal.media.browser.selectMediaAndSubmit([existingFile]);
      }
    });
  }
}

}(jQuery));
